<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js">
    <script src="Assest/bootstrap-3.3.7-dist/js/jquery.min.js"></script>
    <script src="Assest/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>A modal to show a form where user will select a picture (FILE) to upload with description in a
        text field and also date picker</h2>
    <!-- Trigger the modal with a button -->
    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">click for  Modal</button>

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="document">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="Submit" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Upload a file with Description and Date</h4>
                </div>
                <div class="modal-body">
                    <p>

                        <!DOCTYPE html>


                        <form action="Assest" method="post" enctype="multipart/form-data">
                            Select image to upload:
                            <input type="file" name="fileToUpload" id="fileToUpload">

                        </form>

                        <div>
                            <input type="date">
                        </div>
                    <div>
                        <input type="textfild">
                    </div>


                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Submit</button>
                </div>
            </div>

        </div>
    </div>

</div>

</body>
</html>