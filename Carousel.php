<?php
$image_name= time().$_FILES['image']['name'];
$temporary_location= $_FILES['image']['tmp_name'];

$myLocation = 'image/'.$image_name;

move_uploaded_file($temporary_location,$myLocation);

$date = $_POST['date_of_upload'];

echo "The name of the image : ".$_FILES['image']['name']."<br>";
echo "The temporary location of the image : ".$_FILES['image']['tmp_name']."<br>";
echo "The error of the image : ".$_FILES['image']['error']."<br>";
echo "The size of the image : ".$_FILES['image']['size']."<br>";
echo "The type of the image : ".$_FILES['image']['type']."<br>";
echo "<img src= $myLocation>";

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="bootstrap/js/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <style>
        .carousel-inner > .item > img,
        .carousel-inner > .item > a > img {
            width: 70%;
            margin: auto;
        }
    </style>
</head>
<body>

<div class="container">
    <br>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

            <div class="item active">
                <?php echo "<img src= $myLocation>"; ?>
                <div class="carousel-caption">
                    <h3><?php echo $date; ?></h3>
                    <p><?php echo $image_name; ?></p>
                </div>
            </div>




        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>

</body>
</html>
